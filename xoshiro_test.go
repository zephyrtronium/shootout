package shootout_test

import (
	"testing"

	. "gitlab.com/zephyrtronium/shootout"
)

func BenchmarkXoshiro(b *testing.B) {
	b.SetBytes(8)
	x := NewXoshiro()
	var t uint64
	for n := b.N; n > 0; n-- {
		t += x.Uint64()
	}
	Sink = t
}
