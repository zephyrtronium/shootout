package shootout_test

import (
	"testing"

	. "gitlab.com/zephyrtronium/shootout"
)

func BenchmarkMT64(b *testing.B) {
	b.SetBytes(8)
	p := NewMT64()
	p.Seed(1)
	var t uint64
	for n := b.N; n > 0; n-- {
		t += p.Uint64()
	}
	Sink = t
}
