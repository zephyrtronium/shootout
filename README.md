# shootout

PRNG throughput benchmarks. Includes the following PRNGs:

- ChaCha20, a cryptographically secure generator with wide popularity
- ChaCha8, a cryptographically secure generator with less popularity
- MT-64, a variant of Mersenne Twister with 311-dimensional k-distribution to 64-bit accuracy
- PCG-DXSM, an excellent quality, fast 128-bit generator
- Wyrand, a good quality, extremely fast 64-bit generator
- Xoshiro256\*\*, an excellent quality, fast 256-bit generator

The implementations of ChaCha8 and PCG-DXSM were taken from the Go [math/rand/v2](https://go-review.googlesource.com/c/go/+/516859/3) proposal implementation.
ChaCha8 and ChaCha20 have handwritten assembly versions for amd64 and arm64.
The other algorithms are optimized such that they would not benefit from handwritten assembly.

```
goos: windows
goarch: amd64
pkg: gitlab.com/zephyrtronium/shootout
cpu: AMD Ryzen 9 7900X 12-Core Processor
BenchmarkChaCha20-24            343620372                3.530 ns/op    2266.26 MB/s
BenchmarkChaCha8-24             696915510                1.740 ns/op    4596.97 MB/s
BenchmarkMT64-24                528607347                2.225 ns/op    3595.64 MB/s
BenchmarkPCG_DXSM-24            805440481                1.468 ns/op    5450.26 MB/s
BenchmarkWyrand-24              1000000000               0.5433 ns/op   14725.19 MB/s
BenchmarkXoshiro-24             1000000000               0.7732 ns/op   10346.94 MB/s
PASS
ok      gitlab.com/zephyrtronium/shootout       7.228s
```
