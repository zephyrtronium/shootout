package shootout

import (
	"crypto/rand"
	"encoding/binary"
	"math/bits"
)

// Xoshiro is the randomness source type for xirho, an implementation of
// xoshiro256** by David Blackman and Sebastiano Vigna.
type Xoshiro struct {
	w, x, y, z uint64
}

// NewXoshiro produces a new, uniquely seeded RNG.
func NewXoshiro() Xoshiro {
	r := Xoshiro{}
	b := make([]byte, 8*4)
	// Loop to ensure the state is never all-zero, which is an invalid state
	// for xoshiro.
	for r.w == 0 && r.x == 0 && r.y == 0 && r.z == 0 {
		rand.Read(b)
		r.w = binary.LittleEndian.Uint64(b)
		r.x = binary.LittleEndian.Uint64(b[8:])
		r.y = binary.LittleEndian.Uint64(b[16:])
		r.z = binary.LittleEndian.Uint64(b[24:])
	}
	return r
}

// Uint64 produces a 64-bit pseudo-random value.
func (rng *Xoshiro) Uint64() uint64 {
	w, x, y, z := rng.w, rng.x, rng.y, rng.z
	r := bits.RotateLeft64(x*5, 7) * 9
	t := x << 17
	y ^= w
	z ^= x
	x ^= y
	w ^= z
	y ^= t
	z = bits.RotateLeft64(z, 45)
	rng.w, rng.x, rng.y, rng.z = w, x, y, z
	return r
}
