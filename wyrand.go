package shootout

import "math/bits"

type Wyrand struct {
	x uint64
}

func (w *Wyrand) Uint64() uint64 {
	w.x += 0xa0761d6478bd642f
	hi, lo := bits.Mul64(w.x, w.x^0xe7037ed1a0b428db)
	return hi ^ lo
}
