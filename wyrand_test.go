package shootout_test

import (
	"testing"

	. "gitlab.com/zephyrtronium/shootout"
)

func BenchmarkWyrand(b *testing.B) {
	b.SetBytes(8)
	var p Wyrand
	var t uint64
	for n := b.N; n > 0; n-- {
		t += p.Uint64()
	}
	Sink = t
}
