// Copyright 2023 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package shootout_test

import (
	"testing"

	. "gitlab.com/zephyrtronium/shootout"
)

func BenchmarkChaCha20(b *testing.B) {
	b.SetBytes(8)
	p := NewChaCha20([32]byte{1, 2, 3, 4, 5})
	var t uint64
	for n := b.N; n > 0; n-- {
		t += p.Uint64()
	}
	Sink = t
}
