package shootout

import (
	"encoding/binary"
	"unsafe"
)

const (
	mt64N        = 312
	mt64M        = 156
	mt64A uint64 = 0xB5026F5AA96619E9
)

// MT64 is a 64-bit Mersenne twister. The choice of parameters yields a period
// of 2**19937 - 1.
//
// Compared to xoshiro256**, MT64-19937 is far better distributed in higher
// dimensions and has much larger period. However, it is also much slower.
// MT64 is slow to recover from states with small Hamming  weight.
type MT64 struct {
	i int
	s [mt64N]uint64
}

// NewMT64 produces an unseeded 64-bit Mersenne twister. Call Seed[IV]() or
// Restore() prior to use.
func NewMT64() *MT64 {
	return &MT64{}
}

// Seed initializes mt using an int64. This exists to satisfy the rand.Source
// interface.
func (mt *MT64) Seed(seed int64) {
	mt.s[0] = uint64(seed)
	for i := 1; i < mt64N; i++ {
		mt.s[i] = 6364136223846793005*(mt.s[i-1]^mt.s[i-1]>>62) + uint64(i)
	}
	mt.i = mt64N
}

// SeedIV initializes the generator using all bits of iv, which may be of any
// size or nil.
func (mt *MT64) SeedIV(iv []byte) {
	mt.Seed(19650218)
	if len(iv) == 0 {
		return
	}
	if len(iv)&7 != 0 {
		// We need a multiple of 8, but we don't really own iv, so we'll make a
		// copy with the right length.
		t := make([]byte, len(iv)+8-len(iv)&7)
		copy(t, iv)
		iv = t
	}
	k := mt64N
	if mt64N < len(iv) {
		k = len(iv)
	}
	i := 1
	for j := 0; k > 0; k-- {
		mt.s[i] = (mt.s[i]^mt.s[i-1]^mt.s[i-1]>>62)*3935559000370003845 + binary.LittleEndian.Uint64(iv[j*8:]) + uint64(j)
		if i++; i >= mt64N {
			mt.s[0] = mt.s[mt64N-1]
			i = 1
		}
		if j++; j*8 >= len(iv) {
			j = 0
		}
	}
	for k = mt64N - 1; k > 0; k-- {
		mt.s[i] = (mt.s[i]^mt.s[i-1]^mt.s[i-1]>>62)*2862933555777941757 - uint64(i)
		if i++; i >= mt64N {
			mt.s[0] = mt.s[mt64N-1]
			i = 1
		}
	}
}

// Uint64 produces a 64-bit pseudo-random value.
func (mt *MT64) Uint64() uint64 {
	if mt.i >= mt64N {
		i := 0
		for i < mt64N-mt64M {
			x := mt.s[i]&0xffffffff80000000 | mt.s[i+1]&0x000000007fffffff
			x = x>>1 ^ mt64A*(x&1)
			mt.s[i] = mt.s[i+mt64M] ^ x
			i++
		}
		for i < mt64N-1 {
			x := mt.s[i]&0xffffffff80000000 | mt.s[i+1]&0x000000007fffffff
			x = x>>1 ^ mt64A*(x&1)
			y := *(*uint64)(unsafe.Add(unsafe.Pointer(&mt.s[0]), 8*(i-(mt64N-mt64M))))
			mt.s[i] = y ^ x
			i++
		}
		x := mt.s[mt64N-1]&0xffffffff80000000 | mt.s[0]&0x000000007fffffff
		x = x>>1 ^ mt64A*(x&1)
		mt.s[mt64N-1] = mt.s[mt64M-1] ^ x
		mt.i = 0
	}

	x := *(*uint64)(unsafe.Add(unsafe.Pointer(&mt.s[0]), 8*mt.i))
	mt.i++
	x ^= x >> 29 & 0x5555555555555555
	x ^= x << 17 & 0x71D67FFFEDA60000
	x ^= x << 37 & 0xFFF7EEE000000000
	x ^= x >> 43
	return x
}
